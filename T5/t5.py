def main():
    V = int(input())
    n = int(input())
    intervals = read_intervals(n)
    intervals.sort(key=lambda tup : tup[0])
    solution = solve(intervals, V)
    print_solution(solution)


def read_intervals(n):
    intervals = []
    for _ in range(n):
        interval = tuple([int(x) for x in input().split()])
        intervals.append(interval)

    return intervals


def solve(intervals, V):
    n = len(intervals)
    solution = []
    current_interval_idx = 0
    current_chosen_interval = 0
    already_covered_value = 0

    while already_covered_value < V:
        while intervals[current_interval_idx][0] <= already_covered_value:
            if intervals[current_chosen_interval][1] < intervals[current_interval_idx][1]:
                current_chosen_interval = current_interval_idx

            current_interval_idx += 1
            if current_interval_idx == n:
                if intervals[current_chosen_interval][1] < V:
                    return None
                else:
                    break

        chosen = intervals[current_chosen_interval]
        if chosen[1] <= already_covered_value:
            return None

        solution.append(chosen)
        already_covered_value = chosen[1]

    return solution


def print_solution(solution):
    if solution:
        print(len(solution))
        for interval in solution:
            print(interval[0], interval[1])
    else:
        print(0)

if __name__ == '__main__':
    main()
