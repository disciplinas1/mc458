#!/bin/bash

for n in 01 02 03 04 05 06 07 08 09 10; do
    python t5.py < testes/arq${n}.in > testes/arq${n}.txt
    diff testes/arq${n}.txt testes/arq${n}.res
done
