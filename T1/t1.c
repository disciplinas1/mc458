#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

void biggest_sum_limits(int values[], unsigned size) {
    unsigned start_index = 0;
    unsigned end_index = 0;
    unsigned current_start_index = 0;
    int current_sum = 0;
    int max = INT32_MIN;

    for (unsigned k = 0; k < size; k++) {
        current_sum += values[k];

        if (
            (current_sum > max) ||
            ((current_sum == max) && (k - current_start_index > end_index - start_index))
        ) {
            max = current_sum;
            start_index = current_start_index;
            end_index = k;
        }

        if (current_sum < 0) {
            current_sum = 0;
            current_start_index = k + 1;
        }
    }

    if (max > 0) {
        printf("%u %u\n", start_index + 1, end_index + 1);
    } else {
        printf("0 0\n");
    }
}

int main(void) {
    unsigned n;
    scanf("%u", &n);

    int values[n - 1];
    for (unsigned k = 0; k < n - 1; k++) {
        scanf("%d", &values[k]);
    }

    biggest_sum_limits(values, n - 1);
}
