#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>

typedef uint16_t integer;
#define MAX_INTEGER UINT16_MAX

integer sum(integer a, integer b) {
    if (a == MAX_INTEGER) {
        return MAX_INTEGER;
    } else {
        return a + b;
    }
}

integer max(integer array[], integer size) {
    integer max = array[0];
    for (integer i = 1; i < size; i++) {
        if (array[i] > max) max = array[i];
    }

    return max;
}

void solve(integer value, integer notes_values[], integer n) {
    integer max_value = value + max(notes_values, n);

    integer previous[max_value + 1];
    integer current[max_value + 1];
    previous[0] = 0;
    current[0] = 0;
    for (integer d = 1; d <= max_value; d++) previous[d] = MAX_INTEGER;

    for (integer line_index = 1; line_index <= n; line_index++) {
        integer current_note = notes_values[line_index - 1];
        for (integer current_value = 1; current_value <= max_value; current_value++) {
            if (current_note > current_value) {
                current[current_value] = previous[current_value];
            } else {
                integer minimum = MAX_INTEGER;
                integer k = 0;
                while (k * current_note <= current_value) {
                    integer possible_solution = sum(previous[current_value - k * current_note], k);
                    if (possible_solution < minimum) minimum = possible_solution;
                    k++;
                }

                // printf("%" PRIu16 " ", line_index);
                // printf("%" PRIu16 " ", current_value);
                // printf("%" PRIu16 " ", minimum);
                // printf("\n");
                current[current_value] = minimum;
            }
        }

        for (integer d = 0; d <= max_value; d++) previous[d] = current[d];
    }

    for (integer current_value = value; current_value <= max_value; current_value++) {
        if (current[current_value] != MAX_INTEGER) {
            printf("%" PRIu16 " %" PRIu16 "\n", current_value, current[current_value]);
            break;
        }
    }

    // for (integer line_index = 0; line_index <= n; line_index++) {
    //     for (integer current_value = 0; current_value <= max_value; current_value++) {
    //         integer value = solutions[line_index][current_value];
    //         if (value == MAX_INTEGER) {
    //             printf("X ");
    //         } else {
    //             printf("%" PRIu16 " ", value);
    //         }
    //     }
    //     printf("\n");
    //
}

int main(void) {
    integer value, n;
    scanf("%" SCNu16, &value);
    scanf("%" SCNu16, &n);
    integer notes_values[n];
    for (integer i = 0; i < n; i++) {
        scanf("%" SCNu16, &notes_values[i]);
    }

    // integer value = 10, n = 1;
    // integer notes_values[1] = {1};

    // integer value = 48888, n = 80;
    // integer notes_values[80] = {1972, 1955, 1938, 1904, 1870, 1853, 1836, 1819, 1785, 1768, 1751, 1734, 1700, 1683, 1666, 1632, 1615, 1598, 1581, 1564, 1530, 1513, 1496, 1462, 1428, 1411, 1377, 1360, 1343, 1326, 1292, 1275, 1258, 1224, 1207, 1190, 1156, 1105, 1071, 1037, 1003, 986, 969, 952, 935, 918, 884, 816, 782, 765, 748, 714, 697, 680, 663, 629, 595, 561, 510, 493, 459, 442, 425, 391, 357, 340, 323, 289, 272, 255, 221, 204, 187, 170, 153, 136, 119, 102, 85, 51};

    solve(value, notes_values, n);
    return EXIT_SUCCESS;
}
