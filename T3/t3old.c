#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>
#include <ctype.h>

#define VALUE_SIZE 26

typedef uint8_t byte;
typedef byte account[VALUE_SIZE];

void skip_stdin_line() {
    char *line = NULL;
    size_t len;
    getline(&line, &len, stdin);
}

byte read_hex_char(char input) {
    char hex_input[2] = { input, '\0' };
    return strtol(hex_input, NULL, 16);
}

void read_input(account values[], size_t n) {
    for (size_t i = 0; i < n; i++) {
        char *line = NULL;
        size_t len;
        getline(&line, &len, stdin);
        char *read_char = line;
        size_t write_index = 0;
        while (*read_char != '\n') {
            if (*read_char != ' ') {
                values[i][write_index] = read_hex_char(*read_char);
                write_index++;
            }

            read_char++;
        }

        free(line);
    }
}

void print_output(account values[], size_t n) {
    for (size_t i = 0; i < n; i++) {
        size_t current_index = 0;
        for (size_t j = 0; j < VALUE_SIZE; j++) {
            if (j == 2 || j == 10 || j == 15 || j == 20 || j == 25) {
                printf(" ");
            } else {
                char output[2];
                sprintf(output, "%x", values[i][current_index] & 0xff);
                printf("%c", toupper(output[0]));
                current_index++;
            }
        }

        printf("\n");
    }
}

void radix_sort(account values[], size_t n) {
    byte **aux = calloc(n, sizeof(byte) * VALUE_SIZE);
    for (size_t index = 0; index < VALUE_SIZE; index++) {
        unsigned buckets[16] = { 0 };
        // conta quantos com aquele char naquela posicao
        for (size_t val = 0; val < n; val++) {
            buckets[values[val][index]]++;
        }

        // faz o cumulativo
        for (size_t val = 1; val < n; val++) {
            buckets[val] += buckets[val - 1];
        }

        // preenche b
        for (size_t val = n - 1; val >= 0; val--) {
            buckets[values[val][index]]--;
            for (size_t copy_index = 0; copy_index < VALUE_SIZE; copy_index++) {
                aux[values[val][index]][copy_index] = values[val][copy_index];
            }
        }

        // copia b pro vetor
        for (size_t val = 0; val < n; val++) {
            for (size_t copy_index = 0; copy_index < VALUE_SIZE; copy_index++) {
                values[val][copy_index] = aux[val][copy_index];
            }
        }
    }

    free(aux);
}

int main(void) {
    size_t n;
    scanf("%zu", &n);
    account values[n];
    skip_stdin_line();
    read_input(values, n);
    radix_sort(values, n);
    print_output(values, n);
    return EXIT_SUCCESS;
}
