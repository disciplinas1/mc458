#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>
#include <ctype.h>
#include <stdbool.h>

#define VALUE_SIZE 26

typedef uint8_t byte;

typedef struct {
    byte number[VALUE_SIZE];
} account;

bool is_equal(account first, account second) {
    for (size_t i = 0; i < VALUE_SIZE; i++) {
        if (first.number[i] != second.number[i]) {
            return false;
        }
    }

    return true;
}

void skip_stdin_line() {
    char *line = NULL;
    size_t len;
    getline(&line, &len, stdin);
}

byte read_hex_char(char input) {
    char hex_input[2] = { input, '\0' };
    return strtol(hex_input, NULL, 16);
}

void print_char(byte value) {
    char output[2];
    sprintf(output, "%x", value & 0xff);
    printf("%c", toupper(output[0]));
}

account *read_input(size_t size) {
    account *accounts = malloc(size * sizeof(account));
    for (size_t i = 0; i < size; i++) {
        char *line = NULL;
        size_t len;
        getline(&line, &len, stdin);
        char *read_char = line;
        size_t write_index = 0;
        while (*read_char != '\n') {
            if (*read_char != ' ') {
                accounts[i].number[write_index] = read_hex_char(*read_char);
                write_index++;
            }

            read_char++;
        }

        free(line);
    }

    return accounts;
}

void print_account(account value) {
    size_t current_index = 0;
    for (size_t j = 0; j < VALUE_SIZE + 5; j++) {
        if (j == 2 || j == 11 || j == 16 || j == 21 || j == 26) {
            printf(" ");
        } else {
            print_char(value.number[current_index]);
            current_index++;
        }
    }
}

void print_output(account *accounts, size_t size) {
    account last_account = accounts[0];
    unsigned distinct_count = 1;
    for (size_t i = 1; i < size; i++) {
        if (!is_equal(last_account, accounts[i])) {
            distinct_count++;
            last_account = accounts[i];
        }
    }

    printf("%u\n", distinct_count);

    unsigned equal_count = 1;
    last_account = accounts[0];
    for (size_t i = 1; i < size; i++) {
        if (is_equal(last_account, accounts[i])) {
            equal_count++;
        } else {
            print_account(last_account);
            printf(" %u\n", equal_count);
            last_account = accounts[i];
            equal_count = 1;
        }
    }

    print_account(last_account);
    printf(" %u\n", equal_count);
}

void radix_sort(account *accounts, size_t size) {
    account *aux = calloc(size, sizeof(account));
    for (int index = VALUE_SIZE - 1; index >= 0; index--) {
        unsigned buckets[16] = { 0 };

        // conta quantos com aquele char naquela posicao
        for (size_t val = 0; val < size; val++) {
            buckets[accounts[val].number[index]]++;
        }

        // faz o cumulativo
        for (size_t val = 1; val < 16; val++) {
            buckets[val] += buckets[val - 1];
        }

        // preenche aux
        for (int val = size - 1; val >= 0; val--) {
            byte char_value = accounts[val].number[index];
            buckets[char_value]--;
            aux[buckets[char_value]] = accounts[val];
        }

        // copia b pro vetor
        for (size_t val = 0; val < size; val++) {
            accounts[val] = aux[val];
        }
    }

    free(aux);
}

int main(int argc, char *argv[]) {
    size_t size;
    scanf("%zu", &size);
    skip_stdin_line();
    account *accounts = read_input(size);
    radix_sort(accounts, size);
    print_output(accounts, size);
    free(accounts);
    return EXIT_SUCCESS;
}
