#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

unsigned read_value() {
    unsigned value;
    scanf("%u", &value);
    return value;
}

typedef struct node {
    unsigned value;
    struct node *next;
} node;

typedef struct linked_list {
    node *head;
} linked_list;

linked_list create_list(unsigned size) {
    node *head = NULL;
    for (unsigned index = 0; index < size; index++) {
        node *new = (node *) malloc(sizeof(node));
        new->value = read_value();
        new->next = head;
        head = new;
    }

    return (linked_list) { .head = head };
}

void free_list(linked_list *list) {
    node *current = list->head;
    while (current != NULL) {
        node *next = current->next;
        free(current);
        current = next;
    }

    list->head = NULL;
}

void print_list(const linked_list list) {
    node *current = list.head;
    while (current != NULL) {
        printf("%u ", current->value);
        current = current->next;
    }

    printf("\n");
}

unsigned solve_flips(linked_list *list, unsigned size) {
    if (size == 1) {
        free(list->head);
        return 0;
    }

    unsigned max_index = 0;
    if (list->head->value == size) {
        node *to_free = list->head;
        list->head = to_free->next;
        free(to_free);
    } else {
        max_index = 1;
        node *previous = list->head;
        while (previous->next->value != size) {
            previous = previous->next;
            max_index++;
        }

        node *to_free = previous->next;
        previous->next = to_free->next;
        free(to_free);
    }

    unsigned old_value = solve_flips(list, size - 1);
    return old_value + max_index;
}

int main(void) {
    unsigned n;
    scanf("%u", &n);
    linked_list list = create_list(n);
    unsigned solution = solve_flips(&list, n);
    printf("%u\n", solution);
    return EXIT_SUCCESS;
}
