#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

unsigned merge(unsigned values[], size_t begin, size_t mid, size_t end) {
    unsigned aux[end - begin + 1];
    for (size_t i = begin; i <= mid; i++) {
        aux[i - begin] = values[i];
    }

    for (size_t j = mid + 1; j <= end; j++) {
        aux[end + mid + 1 - j - begin] = values[j];
    }

    size_t i = begin;
    size_t j = end;
    unsigned count = 0;
    for (size_t index = begin; index <= end; index++) {
        if (aux[i - begin] <= aux[j - begin]) {
            values[index] = aux[i - begin];
            i++;
        } else {
            values[index] = aux[j - begin];
            j--;
            count += mid + 1 - i;
        }
    }

    return count;
}

unsigned merge_sort(unsigned values[], size_t begin, size_t end) {
    if (begin >= end) {
        return 0;
    } else {
        size_t mid = (end + begin) / 2;
        unsigned half_1 = merge_sort(values, begin, mid);
        unsigned half_2 = merge_sort(values, mid + 1, end);
        unsigned merge_val = merge(values, begin, mid, end);
        return half_1 + half_2 + merge_val;
    }
}

int main(void) {
    size_t n;
    scanf("%zu", &n);
    unsigned values[n];
    for (size_t i = 0; i < n; i++) {
        scanf("%u", &values[i]);
    }

    unsigned solution = merge_sort(values, 0, n - 1);
    printf("%u\n", solution);
    return EXIT_SUCCESS;
}
